// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "RunCharacterController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"

// Sets default values
ARunCharacter::ARunCharacter()
{

	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CameraArm = CreateDefaultSubobject<USpringArmComponent>("CameraArm");
	CameraArm->SetupAttachment(RootComponent);
	CameraArm->TargetArmLength = 500.0f;
	Camera = CreateAbstractDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(CameraArm);
	isDead = false;

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{

	Super::BeginPlay();
	FVector direction = FVector(1, 0, 0);

}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ARunCharacter::Die()
{
	if (isDead == false)
	{
		ARunCharacterController* playerController = Cast<ARunCharacterController>(GEngine->GetFirstLocalPlayerController(GetWorld()));

		if (playerController != nullptr)
		{

			playerController->Disable();
			this->SetActorHiddenInGame(true);
			OnPlayerDeath.Broadcast();
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticles, GetActorLocation());
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSoundAtLocation(this, SoundCue, GetActorLocation(), FRotator::ZeroRotator, 1.0f, 1.0f, 0.0f, nullptr, nullptr, true);
			isDead = true;
			
		}
	}
}

void ARunCharacter::AddPoints(int32 pointsAmount)
{

	points += pointsAmount;
}
