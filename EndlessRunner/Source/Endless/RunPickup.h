// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Actor.h"
#include "RunPickup.generated.h"

UCLASS()
class ENDLESS_API ARunPickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARunPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* Meshi;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* DefaultScene;

	UPROPERTY(EditAnywhere, Category = "Points")
		USoundCue* SoundCue;

	UPROPERTY(EditAnywhere, Category = "Points")
		int32 pointsAmount;

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
			const FHitResult& SweepResult);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
