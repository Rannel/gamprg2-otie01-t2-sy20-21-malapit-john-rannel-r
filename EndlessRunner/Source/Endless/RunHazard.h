// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunTile.h"
#include "GameFramework/Actor.h"
#include "RunHazard.generated.h"

UCLASS()
class ENDLESS_API ARunHazard : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARunHazard();

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* Meshi;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* DefaultScene;

	UFUNCTION()
	void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
