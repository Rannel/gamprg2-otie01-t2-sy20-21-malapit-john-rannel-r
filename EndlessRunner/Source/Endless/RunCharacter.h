// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharacterDeath);

UCLASS()
class ENDLESS_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	ARunCharacter();

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, Category = "EventDispatcher")
	FOnCharacterDeath OnPlayerDeath;

	UPROPERTY(EditAnywhere, Category = "Death")
	UParticleSystem* DeathParticles;

	UPROPERTY(EditAnywhere, Category = "Death")
		USoundCue* SoundCue;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Points")
		int32 points;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USpringArmComponent* CameraArm;

	bool isDead;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void Die();
	void AddPoints(int32 pointsAmount);
};
