// Fill out your copyright notice in the Description page of Project Settings.


#include "RunHazard.h"
#include "RunCharacter.h"

// Sets default values
ARunHazard::ARunHazard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DefaultScene = CreateDefaultSubobject<USceneComponent>("DefaultScene");
	SetRootComponent(DefaultScene);


	Meshi = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Meshi->SetupAttachment(DefaultScene);

}

// Called when the game starts or when spawned
void ARunHazard::BeginPlay()
{

	Super::BeginPlay();
	OnActorHit.AddDynamic(this, &ARunHazard::OnHit);

}

void ARunHazard::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (ARunCharacter* player = Cast<ARunCharacter>(OtherActor))
	{

		player->Die();

	}
}

// Called every frame
void ARunHazard::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

}

