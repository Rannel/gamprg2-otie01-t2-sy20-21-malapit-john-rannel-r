// Fill out your copyright notice in the Description page of Project Settings.

#include "RunGameMode.h"
#include "Engine/World.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"

void ARunGameMode::BeginPlay()
{

	Super::BeginPlay();
	WorldScene = GetWorld();
	location = FVector(-100, 0, 0);
	rotation = FRotator(0, 0, 0);
	FActorSpawnParameters spawnParams;

	for (int i = 0; i < 10; i++)
	{

		ARunGameMode::AddTile();

	}

	restartLevel = false;

	player = Cast<ARunCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	player->OnPlayerDeath.AddDynamic(this, &ARunGameMode::RestartLevel);
}

void ARunGameMode::AddTile()
{

	BPTile = Cast<ARunTile>(WorldScene->SpawnActor(BP_RunTile, &location, &rotation));
	location = BPTile->AttachTransform();
	BPTile->OnTileExit.AddDynamic(this, &ARunGameMode::OnTileExited);
	
}


void ARunGameMode::OnTileExited(ARunTile* tile, ARunHazard* hazard, ARunPickup* pickup)
{
	
	ARunGameMode::AddTile();
	FTimerHandle timerHandle;
	FTimerDelegate timerDelegate = FTimerDelegate::CreateUObject(this, &ARunGameMode::TileDestroy, tile, hazard, pickup);
	GetWorldTimerManager().SetTimer(timerHandle, timerDelegate, 0.5f, false);
}

void ARunGameMode::TileDestroy(ARunTile* Tile, ARunHazard* Hazard, ARunPickup* Pickup)
{

	Tile->Destroy();

	if (Hazard != nullptr)
	{
		Hazard->Destroy();
	}

	if (Pickup != nullptr)
	{
		Pickup->Destroy();
	}
}

void ARunGameMode::RestartLevel()
{
	if (restartLevel == false)
	{

		FTimerHandle timerHandle;
		FTimerDelegate timerDelegate = FTimerDelegate::CreateUObject(this, &ARunGameMode::Restart);
		GetWorldTimerManager().SetTimer(timerHandle, timerDelegate, 2.0f, false);
		restartLevel = true;

	}
}

void ARunGameMode::Restart()
{

	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);

}

void ARunGameMode::Tick(float DeltaTime)
{

}

