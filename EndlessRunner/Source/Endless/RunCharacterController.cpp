// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacterController.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

void ARunCharacterController::BeginPlay()
{

	Super::BeginPlay();

	runPlayer = Cast<ARunCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (runPlayer == nullptr)
		UE_LOG(LogTemp, Warning, TEXT("Actor is null!"));

	rightDirection = FVector(0, 1, 0);
	forwardDirection = FVector(1, 0, 0);

}

void ARunCharacterController::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	runPlayer->AddMovementInput(forwardDirection, 1.0f);

}

void ARunCharacterController::Enable()
{

	EnableInput(Cast<ARunCharacterController>(this));

}

void ARunCharacterController::Disable()
{

	DisableInput(Cast<ARunCharacterController>(this));
	forwardDirection = FVector(0, 0, 0);

}

void ARunCharacterController::SetupInputComponent()
{

	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::DirectionalMoveFunction);

}

void ARunCharacterController::DirectionalMoveFunction(float axisValue)
{

	runPlayer->AddMovementInput(rightDirection, axisValue);

}