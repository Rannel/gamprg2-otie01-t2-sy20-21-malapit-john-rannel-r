// Fill out your copyright notice in the Description page of Project Settings.


#include "RunPickup.h"
#include "RunCharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
// Sets default values
ARunPickup::ARunPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DefaultScene = CreateDefaultSubobject<USceneComponent>("DefaultScene");
	SetRootComponent(DefaultScene);


	Meshi = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	Meshi->SetupAttachment(DefaultScene);

}

// Called when the game starts or when spawned
void ARunPickup::BeginPlay()
{
	Super::BeginPlay();

	Meshi->OnComponentBeginOverlap.AddDynamic(this, &ARunPickup::OnOverlapBegin);
}



void ARunPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* player = Cast<ARunCharacter>(OtherActor))
	{

		player->AddPoints(pointsAmount);
		UAudioComponent* AudioComponent = UGameplayStatics::SpawnSoundAtLocation(this, SoundCue, GetActorLocation(), FRotator::ZeroRotator, 1.0f, 1.0f, 0.0f, nullptr, nullptr, true);
		this->Destroy();

	}
}

// Called every frame
void ARunPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

