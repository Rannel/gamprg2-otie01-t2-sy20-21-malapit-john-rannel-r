// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunTile.h"
#include "RunHazard.h"
#include "RunCharacter.h"
#include "RunPickup.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

UCLASS()
class ENDLESS_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

	ARunTile* BPTile;

	UPROPERTY(EditAnywhere, Category = "Tile")
	TSubclassOf<class ARunTile> BP_RunTile;

	FVector location;
	FRotator rotation;
	UWorld* WorldScene;

	ARunCharacter* player;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool restartLevel;
public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddTile();

	UFUNCTION()
	void OnTileExited(ARunTile* tile, ARunHazard* hazard, ARunPickup* pickup);

	void TileDestroy(ARunTile* Tile, ARunHazard* Hazard, ARunPickup* Pickup);

	UFUNCTION()
		void RestartLevel();

	void Restart();
};
