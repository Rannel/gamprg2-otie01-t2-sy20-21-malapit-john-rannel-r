// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunCharacter.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

UCLASS()
class ENDLESS_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	ARunCharacter* runPlayer;
	FVector rightDirection;
	FVector forwardDirection;
	void DirectionalMoveFunction(float axisValue);

public:

	virtual void Tick(float DeltaTime) override;

	void Enable();
	void Disable();
};
