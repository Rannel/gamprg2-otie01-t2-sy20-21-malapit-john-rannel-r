// Fill out your copyright notice in the Description page of Project Settings.

#include "RunTile.h"
#include "RunCharacter.h"
#include "RunGameMode.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ARunTile::ARunTile()
{

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DefaultScene = CreateDefaultSubobject<USceneComponent>("DefaultScene");
	SetRootComponent(DefaultScene);

	Floor = CreateDefaultSubobject<UStaticMeshComponent>("Floor");
	Floor->SetupAttachment(DefaultScene);

	LeftWall = CreateDefaultSubobject<UStaticMeshComponent>("LeftWall");
	LeftWall->SetupAttachment(DefaultScene);

	RightWall = CreateDefaultSubobject<UStaticMeshComponent>("RightWall");
	RightWall->SetupAttachment(DefaultScene);

	AttachPointArrow = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	AttachPointArrow->SetupAttachment(DefaultScene);

	TileDespawnPoint = CreateDefaultSubobject<UBoxComponent>("TileDespawnPoint");
	TileDespawnPoint->SetupAttachment(DefaultScene);

	HazardSpawnPoint = CreateDefaultSubobject<UBoxComponent>("HazardSpawnPoint");
	HazardSpawnPoint->SetupAttachment(DefaultScene);

	ItemSpawnPoint = CreateDefaultSubobject<UBoxComponent>("ItemSpawnPoint");
	ItemSpawnPoint->SetupAttachment(DefaultScene);

}

// Called when the game starts or when spawned
void ARunTile::BeginPlay()
{

	Super::BeginPlay();
	
	TileDespawnPoint->OnComponentBeginOverlap.AddDynamic(this, &ARunTile::OnOverlapBegin);

	
	//ARunTile::SpawnPickup();

	int32 rand;
	rand = FMath::RandRange(0, 3);

	if (rand == 0)
	{

	}
	else if (rand == 1)
	{

		ARunTile::SpawnHazard();
		ARunTile::SpawnPickup();

	}
	else if (rand == 2)
	{
		
		ARunTile::SpawnHazard();

	}
	else if (rand == 3)
	{

		ARunTile::SpawnPickup();
		
	}


}

void ARunTile::PickRandomHazard()
{

	int32 arrayCount = BP_RunHazardArray.Num() - 1;
	
	if (BP_RunHazardArray.Num() > 0)
	{

		int32 selectedActor = FMath::RandRange(0, arrayCount);
		BP_RunHazard = BP_RunHazardArray[selectedActor];

	}

}

void ARunTile::SpawnHazard()
{

	const FVector Location = GetActorLocation();
	const FRotator Rotation = GetActorRotation();
	ARunTile::PickRandomHazard();

	Hazard = Cast<ARunHazard>(GetWorld()->SpawnActor<ARunHazard>(BP_RunHazard, Location, Rotation));
	Hazard->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	FVector hazardSpawnPointExtent = HazardSpawnPoint->GetScaledBoxExtent();
	FVector hazardSpawnPointLocation = HazardSpawnPoint->GetRelativeLocation();

	hazardSpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(hazardSpawnPointLocation, hazardSpawnPointExtent);
	Hazard->SetActorRelativeLocation(hazardSpawnLocation);

}

void ARunTile::PickRandomPickup()
{
	int32 arrayCount = BP_RunPickupArray.Num() - 1;

	if (BP_RunPickupArray.Num() > 0)
	{

		int32 selectedActor = FMath::RandRange(0, arrayCount);
		BP_RunPickup = BP_RunPickupArray[selectedActor];

	}
}

void ARunTile::SpawnPickup()
{

	const FVector Location = GetActorLocation();
	const FRotator Rotation = GetActorRotation();
	ARunTile::PickRandomPickup();

	Pickup = Cast<ARunPickup>(GetWorld()->SpawnActor<ARunPickup>(BP_RunPickup, Location, Rotation));
	Pickup->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	FVector pickupSpawnPointExtent = ItemSpawnPoint->GetScaledBoxExtent();
	FVector pickupSpawnPointLocation = ItemSpawnPoint->GetRelativeLocation();

	pickupSpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(pickupSpawnPointLocation, pickupSpawnPointExtent);
	Pickup->SetActorRelativeLocation(pickupSpawnLocation);

}

// Called every frame
void ARunTile::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

}

FVector ARunTile::AttachTransform()
{

	FVector AttachPoint = GetTransform().InverseTransformPosition(AttachPointArrow->GetComponentLocation());
	FVector worldLoc = GetTransform().TransformPosition(AttachPoint);

	return worldLoc;

}

void ARunTile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	OnTileExit.Broadcast(this, Hazard, Pickup);

}