// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunHazard.h"
#include "RunPickup.h"
#include "GameFramework/Actor.h"
#include "RunTile.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FTileExitedSignature, class ARunTile*, targetTile, class ARunHazard*, targetHazard, class ARunPickup*, targetPickup);

UCLASS()
class ENDLESS_API ARunTile : public AActor
{
	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	ARunTile();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	ARunHazard* Hazard;

	UPROPERTY(EditAnywhere, Category = "Hazard")
		TSubclassOf<class ARunHazard> BP_RunHazard;

	UPROPERTY(EditAnywhere, Category = "Hazard")
		TArray<TSubclassOf<ARunHazard>> BP_RunHazardArray;


	ARunPickup* Pickup;

	UPROPERTY(EditAnywhere, Category = "Pickup")
		TSubclassOf<class ARunPickup> BP_RunPickup;

	UPROPERTY(EditAnywhere, Category = "Pickup")
		TArray<TSubclassOf<ARunPickup>> BP_RunPickupArray;

	//TArray<class ARunHazard> BP_Test;
	FVector hazardSpawnLocation;
	FVector pickupSpawnLocation;

	UPROPERTY(BlueprintAssignable, VisibleAnywhere, Category = "EventDispatcher")
		FTileExitedSignature OnTileExit;

	//
	FVector AttachTransform();

	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
			const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* Floor;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* LeftWall;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* RightWall;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* AttachPointArrow;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* TileDespawnPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* HazardSpawnPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* ItemSpawnPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* DefaultScene;

	void PickRandomHazard();

	void SpawnHazard();

	void PickRandomPickup();
	
	void SpawnPickup();
};
